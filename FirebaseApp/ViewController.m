//
//  ViewController.m
//  FirebaseApp
//
//  Created by osx on 05/08/16.
//  Copyright © 2016 Ameba Technologies. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    Firebase *mainURL;
    UIAlertView *alertView;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    mainURL = [[Firebase alloc] initWithUrl:@"learnfirebaseapp.firebaseIO.com"];
    
    [mainURL setValue:@"My Firebase data"];
    
    [mainURL observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot)
    {
        NSLog(@"%@ -> %@", snapshot.key, snapshot.value);
    }];
    
    alertView = [[UIAlertView alloc] initWithTitle:nil message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
}

- (IBAction)logInBtnAction:(id)sender
{
    Firebase *ref = [[Firebase alloc] initWithUrl:@"learnfirebaseapp.firebaseIO.com"];
    [ref authUser:_emailTextField.text password:_passwordTextField.text
withCompletionBlock:^(NSError *error, FAuthData *authData)
    {
    
    if (error)
    {
        alertView.message = @"Wrong username and password";
    }
    else
    {
        alertView.message = @"Successful login";
        
        NSDictionary *alanisawesome = @{
                                        @"full_name" : @"Alan Turing",
                                        @"date_of_birth": @"June 23, 1912"
                                        };
        NSDictionary *gracehop = @{
                                   @"full_name" : @"Grace Hopper",
                                   @"date_of_birth": @"December 9, 1906"
                                   };
        
        NSDictionary *users = @{
                                @"alanisawesome": alanisawesome,
                                @"gracehop": gracehop
                                };
        [mainURL setValue: users];
        
    }
        
        [alertView show];
}];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
