//
//  createAccountViewController.h
//  FirebaseApp
//
//  Created by osx on 05/08/16.
//  Copyright © 2016 Ameba Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Firebase/Firebase.h>

@interface createAccountViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;

@property (strong, nonatomic) IBOutlet UITextField *againPasswordTextField;

@property (strong, nonatomic) IBOutlet UITextField *enterMobileTextField;
@end
